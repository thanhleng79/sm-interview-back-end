package vn.thanhle.sminterview.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class ClientInfoResponse {
    private String fullUrl;
    private String ipAddress;
    private String operationSystem;
    private String browserName;
    private String referrer;
    private String userAgent;
    private String timeAccess;
}
