package vn.thanhle.sminterview.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "client_visitor")
public class ClientVisitor {
    @Id
    @GeneratedValue
    private Long id;
    @Column(unique=true)
    private String ipAddress;
    private String browserInfo;
    private String operation;
    private String referrer;
    private String userAgent;
    private String fullUrl;
    private Long visitAmount;
    private Long lastVisitTime;
}
