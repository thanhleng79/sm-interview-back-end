package vn.thanhle.sminterview.service;

import vn.thanhle.sminterview.model.ClientVisitor;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface ClientService {
    ClientVisitor getClientInfo(HttpServletRequest request);
    List<ClientVisitor> getLastHundredVisitors();
    List<ClientVisitor> getMostHundredVisitors();
    ClientVisitor getClientInfoByIp(String ipAddress);
}
