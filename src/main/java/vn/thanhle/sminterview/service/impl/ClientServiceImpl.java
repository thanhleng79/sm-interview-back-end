package vn.thanhle.sminterview.service.impl;

import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import vn.thanhle.sminterview.dto.ClientInfoResponse;
import vn.thanhle.sminterview.model.ClientVisitor;
import vn.thanhle.sminterview.repository.ClientVisitorRepository;
import vn.thanhle.sminterview.service.ClientService;
import vn.thanhle.sminterview.utils.HttpUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Log4j2
@Service
public class ClientServiceImpl implements ClientService {

    private ClientVisitorRepository clientVisitorRepository;

    public ClientServiceImpl(ClientVisitorRepository clientVisitorRepository) {
        this.clientVisitorRepository = clientVisitorRepository;
    }

    @Override
    public ClientVisitor getClientInfo(HttpServletRequest request) {
        String ipAddress = HttpUtils.getClientIpAddr(request);
        //find in database by IP
        Optional<ClientVisitor> clientVisitorOptional = clientVisitorRepository.findByIpAddress(ipAddress);

        //if not exist => save new
        ClientVisitor clientVisitor;
        if (clientVisitorOptional.isPresent()) {
            clientVisitor = clientVisitorOptional.get();
            clientVisitor.setLastVisitTime(System.currentTimeMillis());
            clientVisitor.setVisitAmount(clientVisitor.getVisitAmount() + 1);
        }
        else {
            clientVisitor = new ClientVisitor();
            clientVisitor.setIpAddress(ipAddress);
            clientVisitor.setVisitAmount(1L);
        }
        clientVisitor.setBrowserInfo(HttpUtils.getClientBrowser(request));
        clientVisitor.setOperation(HttpUtils.getClientOS(request));
        clientVisitor.setFullUrl(HttpUtils.getFullURL(request));
        clientVisitor.setReferrer(HttpUtils.getReferer(request));
        clientVisitor.setUserAgent(HttpUtils.getUserAgent(request));
        clientVisitor.setLastVisitTime(System.currentTimeMillis());
        return clientVisitorRepository.save(clientVisitor);
    }

    @Override
    public List<ClientVisitor> getLastHundredVisitors() {
        try {
            return clientVisitorRepository.findLastHundredVisitor();
        } catch (Exception ex) {
            log.error("ex: {}", ex.getMessage());
            return Collections.emptyList();
        }
    }

    @Override
    public List<ClientVisitor> getMostHundredVisitors() {
        try {
            return clientVisitorRepository.findMostHundredVisitor();
        } catch (Exception ex) {
            log.error("ex: {}", ex.getMessage());
            return Collections.emptyList();
        }
    }

    @Override
    public ClientVisitor getClientInfoByIp(String ipAddress) {
        try {
            Optional<ClientVisitor> clientVisitorOptional = clientVisitorRepository.findByIpAddress(ipAddress);
            return clientVisitorOptional.orElse(null);
        } catch (Exception ex) {
            log.error("ex: {}", ex.getMessage());
        }
        return null;
    }
}
