package vn.thanhle.sminterview.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import vn.thanhle.sminterview.model.ClientVisitor;

import java.util.List;
import java.util.Optional;

@Repository
public interface ClientVisitorRepository extends JpaRepository<ClientVisitor, Integer> {
    Optional<ClientVisitor> findByIpAddress(String ipAddress);

    @Query(value = "SELECT * FROM client_visitor " +
            "ORDER BY last_visit_time DESC " +
            "LIMIT 100", nativeQuery = true)
    List<ClientVisitor> findLastHundredVisitor();

    @Query(value = "SELECT * FROM client_visitor " +
            "ORDER BY visit_amount DESC " +
            "LIMIT 100", nativeQuery = true)
    List<ClientVisitor> findMostHundredVisitor();
}
