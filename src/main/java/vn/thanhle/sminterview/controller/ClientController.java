package vn.thanhle.sminterview.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import vn.thanhle.sminterview.dto.ClientInfoResponse;
import vn.thanhle.sminterview.model.ClientVisitor;
import vn.thanhle.sminterview.service.ClientService;
import vn.thanhle.sminterview.utils.HttpUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("api/v1")
public class ClientController {

    @Value("${app.name}")
    private String appName;

    private final ClientService clientService;

    public ClientController(ClientService clientService) {
        this.clientService = clientService;
    }

    @GetMapping("/health")
    public ResponseEntity<String> healthCheck() {
        return ResponseEntity.ok(appName);
    }

    @GetMapping("/client-info")
    public ResponseEntity<ClientVisitor> getClientIPAddress(HttpServletRequest request) {
        return ResponseEntity.ok(clientService.getClientInfo(request));
    }

    @GetMapping("/last-hundred-visitor")
    public ResponseEntity<List<ClientVisitor>> lastHundredVisitors() {
        return ResponseEntity.ok(clientService.getLastHundredVisitors());
    }

    @GetMapping("/most-hundred-visitor")
    public ResponseEntity<List<ClientVisitor>> mostHundredVisitors() {
        return ResponseEntity.ok(clientService.getMostHundredVisitors());
    }

    @GetMapping("/client-info-by-ip")
    public ResponseEntity<ClientVisitor> getClientInfoByIp(@RequestParam String ipAddress) {
        return ResponseEntity.ok(clientService.getClientInfoByIp(ipAddress));
    }

}
